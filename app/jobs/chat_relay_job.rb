class ChatRelayJob < ApplicationJob
  def perform(message)
    data = [ message.id, message.name, message.message, message.created_at ]
    ActionCable.server.broadcast('chat_msg', message: data)
  end
end