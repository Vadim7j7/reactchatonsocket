class ChatChannel < ApplicationCable::Channel

  def subscribed
    stream_from 'chat_msg'
  end

  def receive(data)
    ActionCable.server.broadcast 'chat_msg', data
  end

end