var Message = React.createClass({
    render: function() {
        return (
            <div className="message">
                <h3>{this.props.data[1]}</h3>
                <div>{this.props.children.toString()}</div>
            </div>
        )
    }
});