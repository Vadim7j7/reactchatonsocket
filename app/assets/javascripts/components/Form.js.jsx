var Form = React.createClass({
    sendMessage: function() {
        var data, uName, uMessage;
        uName = this.refs.userName;
        uMessage = this.refs.userMessage;
        data = {
            "user[name]": uName.value,
            "user[message]": uMessage.value
        };

        reqwest({
            url: '/chats',
            type: 'json',
            data: data,
            method: 'post',
            headers: {
                'X-CSRF-Token': document.querySelector('[name="csrf-token"]').content
            }
        }).then(function(resp) {
            uName.value = "";
            uMessage.value = "";
        }).fail(function(err, msg) {
        }).always(function() {});
    },
    handleSubmit: function(event) {
        event.preventDefault();
        this.sendMessage();
    },
    render: function() {
        return (
            <form action="" onSubmit={this.handleSubmit}>
                <div className="form-field">
                    <input type="text" ref="userName" placeholder="Name" />
                </div>
                <div className="form-field">
                    <textarea ref="userMessage" placeholder="Message"></textarea>
                </div>
                <div className="form-field">
                    <input type="submit" value="Send" />
                </div>
            </form>
        )
    }
});