var Response = React.createClass({
    componentDidMount: function() {
        var App, _this;
        App = {};
        _this = this;
        App.cable = ActionCable.createConsumer();
        App.messages = App.cable.subscriptions.create({channel: "ChatChannel", room: "msg"}, {
            connected: function() {
                console.log("Connect success");
            },
            received: function(data) {
                var tmp;
                tmp = _this.state.messages;
                tmp.unshift(data.message);
                _this.setState({ messages: tmp });
            }
        });
    },
    getInitialState: function() {
        return { messages: this.props.messages }
    },
    render: function() {
        return(
            <MessageList data={this.state.messages} />
        );
    }
});