var MessageList = React.createClass({
    render: function() {
        var messageNodes;
        messageNodes = this.props.data.map(function(message) {
            return (
                <Message key={message[0]} data={message}>
                    {message[2]}
                </Message>
            )
        });

        return(
            <div className="messagesList">
                {messageNodes}
            </div>
        );
    }
});