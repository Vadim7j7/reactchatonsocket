class ChatsController < ApplicationController

  def create
    message = Message.new(message_data)
    message.save

    render json: message
  end

  private

  def message_data
    params.require(:user).permit([:name, :message])
  end

end