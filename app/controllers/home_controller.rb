class HomeController < ApplicationController
  def Index
    @messages = Message.all.order(:created_at).reverse_order.limit(20).pluck(:id, :name, :message, :created_at)
  end
end
