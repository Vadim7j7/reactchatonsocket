class Message < ApplicationRecord

  after_commit { ChatRelayJob.perform_later(self) }

end
