Rails.application.configure do
  config.react.addons = true
  config.react.jsx_transform_options = {
      blacklist: %w(spec.functionName validation.react strict),
      optional: ['transformerName'],
      whitelist: ['useStrict']
  }
end
