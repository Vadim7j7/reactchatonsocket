Rails.application.routes.draw do
  root 'home#Index'

  resource :chats, only: [:create]

  mount ActionCable.server => '/cable'
end
